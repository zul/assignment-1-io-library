section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    
        xor rax, rax
    .loop:
        mov dl, [rdi + rax]
        test dl, dl
        jz .ret
        inc rax
        jmp .loop
    .ret: 
        ret
 
    
    
 

; Принимает указатель на нуль-терминированную строку, выводит её в stdout


print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax         	
    mov rsi, rdi           
    mov rax, 1  
    mov rdi, 1       
    syscall
    mov rax, 0
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1         	
    mov rsi, rsp           
    mov rax, 1  
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
    


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
    dec rsp
    mov byte [rsp], 0x0 ;NULL
    mov rax, rdi 
    mov rsi, 10 ;divisor
    mov rcx, 1 ;counter
    .loop:
        xor rdx, rdx
        div rsi
        add rdx, 48
        dec rsp
        mov [rsp], dl
        inc rcx
        test rax, rax
        jz .print
        jmp .loop
    .print:
        mov rdi, rsp
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    add rax, 0 ;to create flags
    js .print_neg
    call print_uint
    jmp .ret
    .print_neg:
        push rdi
        mov rdi, 0x2D
        call print_char
        pop rdi
        neg rdi
        call print_uint
    xor rax, rax
    .ret:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ;rdi and rsi are args
    xor rax, rax
    .loop:

        mov al, [rdi]
        xor al, [rsi]
        jnz .not_equals
        mov al, [rdi]
        test al, al
        jz .equals
        inc rdi
        inc rsi
        jmp .loop
    .not_equals:
        mov rax, 0
        ret
    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov [rsp], byte 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov al, [rsp]
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
     xor rdx, rdx ; counter
     xor rax, rax
     
    .read:
        push rdx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rdx
        
        cmp al, 0x20
        jz .space
        cmp al, 0x9
        jz .space
        cmp al, 0xA
        jz .space

        inc rdx
        cmp rsi, rdx
        js .fail             
        dec rdx
        mov [rdx+rdi], al
        inc rdx
        test al, al
        jz .success
        jmp .read

    .space:
        test rdx, rdx
        jz .read
        jmp .end_word
        
    
    .end_word:
        mov [rdi+rdx], byte 0
        mov rax, rdi
        ret
 
    
    .fail:
        dec rdx
        mov rax, 0
        ret
    .success:
        dec rdx
        mov rax, rdi
        ret
        

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rsi, rsi
    mov rcx, 0 ;
    .loop:
        mov sil, [rdi]
        sub sil, 0x30 ;ascii code of 0
        cmp sil, 0 
        js .end
        cmp sil, 0xa ; 
        jns .end
        inc rdx
        push rdx
        mul rcx
        pop rdx
        add rax, rsi        
        inc rdi
        mov rcx, 10
        jmp .loop
    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    push rdi
    call read_char
    pop rdi
    cmp al, 0x2D
    jz .read_neg
    jmp parse_uint
   .read_neg: 
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx; counter
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    inc rax ; because of 0 terminator
    cmp rdx, rax
    js .fail
    .loop:
        mov r10b, [rdi+rcx]
        mov [rsi+rcx], r10b
        inc rcx
        test r10b, r10b
        jz .success        
        jmp .loop
    .fail:
        mov rax, 0
        ret
    .success:
        ret
